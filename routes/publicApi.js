var express = require('express');
var router = express.Router();
var publicCtrl = require('../app/controllers/publicCtrl');


/* GET users listing. */
router.get('/', function(req, res, next) {
    res.send('respond with a resource');
});

/* POST get PDF. */
router.post('/pdfGenrator',publicCtrl.getPdf);

module.exports = router;

