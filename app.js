var express = require('express');
var app = express();
var index = require('./routes/index');
var public = require('./routes/publicApi');
var bodyParser = require('body-parser');
var path=require('path');

app.use(bodyParser({limit: '50mb'}));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: true}));

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'pug');

app.use('/public', public);
app.use('/index', index);

app.listen(process.env.PORT || 3000, function(){
  console.log("Express server listening on port 3000 in %s mode", app.settings.env);
});