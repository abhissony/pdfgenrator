var CONSTANTS = require('./constants');
// var ERR_HANDLER = require('./errHandler');

var postPdfValidate = function (req) {
    var data = {};

    var errStatus = false;

    for(var key of CONSTANTS.PDF_CONSTANTS.key){
        errStatus = errStatus || !req.body[key];
        if(!req.body[key]){
            data[key]=CONSTANTS.ERR_MESSAGE.REQUIRED_FILLED;
            data['status']=CONSTANTS.HTTP_STATUS.REQUIRED_ERR;
        }
    }

    if(data['status'] && data['status']==CONSTANTS.HTTP_STATUS.REQUIRED_ERR){
        return data;
    }

    if(req.body.postcode && isNaN(req.body.postcode)){
        errStatus = true;
        data['postcode']=CONSTANTS.ERR_MESSAGE.POST_CODE_ERR;
    }
    if(req.body.gender && !(req.body.gender.toLowerCase()=='male' || req.body.gender.toLowerCase()=='female')){
        errStatus = true;
        data['gender']=CONSTANTS.ERR_MESSAGE.GENDER_ERR;
    }

    if(errStatus){
        // data['error']=ERR_HANDLER.postPdfErr(req);
        data['status']=CONSTANTS.HTTP_STATUS.DATA_TYPE_ERR;
    }
    else{
        data['status']=CONSTANTS.HTTP_STATUS.OK;
    }
    return data;
}

exports.postPdfValidate = postPdfValidate;