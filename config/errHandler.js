var CONSTANTS = require('./constants');

var postPdfErr = function (req) {
    var errMsg = {};

    for(var key of CONSTANTS.PDF_CONSTANTS.key){
        if(!req.body[key]){
            errMsg[key]=CONSTANTS.ERR_MESSAGE.REQUIRED_FILLED;
        }
    }

    if(req.body.postcode && isNaN(req.body.postcode)){
        errMsg['Postcode']=CONSTANTS.ERR_MESSAGE.POST_CODE_ERR;
    }
    if(req.body.gender && !(req.body.gender.toLowerCase()=='male' || req.body.gender.toLowerCase()=='female')){
        errMsg['Gender']=CONSTANTS.ERR_MESSAGE.GENDER_ERR;
    }
    return errMsg;
}

exports.postPdfErr = postPdfErr;