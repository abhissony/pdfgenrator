const PDF_CONSTANTS = {
    "heading": "PDF Form",
    "line1": "This is an example of a user fillable PDF form.",
    "line2" : "Please fill out the following fields. Important fields are marked yellow.",
    "title": ['Given Name:','Family Name:','Address:','Postcode:','City:','Country:','Gender:'],
    "key": ['givenName','familyName','address','postcode','city','country','gender'],
    'key_x_position' : 100,
    'value_x_position' : 200,
    'y_position' : 100,
    'line_diff' : 20,
    'heading_font_size' : 18,
    'default_font_size' : 10
};

const HTTP_STATUS = {
    "OK" : 200,
    "DATA_TYPE_ERR" : 400,
    "REQUIRED_ERR" : 401
}

const ERR_MESSAGE = {
    "REQUIRED_FILLED" : 'Please fill this value.',
    "POST_CODE_ERR" : 'Please fill currect value(Number only).',
    "GENDER_ERR" : 'Please fill currect value(male/female).'
}

var obj = { 
    PDF_CONSTANTS: PDF_CONSTANTS,
    HTTP_STATUS: HTTP_STATUS,
    ERR_MESSAGE: ERR_MESSAGE
};

module.exports = obj;