var PDF = require('pdfkit');
var fs = require('fs');
var path=require('path');
var validate = require('../../config/requestHandler');
var CONSTANTS = require('../../config/constants');


/* POST get PDF Ctrl. */
var getPdf = function (req, res) {
    if(validate.postPdfValidate(req).status==CONSTANTS.HTTP_STATUS.OK){
    	generatePdf(req,res);
        // res.send({data:validate.postPdfValidate(req)});
    }else{
	    res.send({data:validate.postPdfValidate(req)});
    }
}

/* generate PDF. */
var generatePdf = function(req,res) {
    doc = new PDF();
    writeStream = fs.createWriteStream(path.resolve(".")+'/pdf/'+req.body.givenName+'.pdf');
    doc.pipe(writeStream);

    doc.fontSize(CONSTANTS.PDF_CONSTANTS.heading_font_size).text(CONSTANTS.PDF_CONSTANTS.heading, CONSTANTS.PDF_CONSTANTS.key_x_position, 40);
    doc.fontSize(CONSTANTS.PDF_CONSTANTS.default_font_size).text(CONSTANTS.PDF_CONSTANTS.line1, CONSTANTS.PDF_CONSTANTS.kay_x_position, 60);
    doc.text(CONSTANTS.PDF_CONSTANTS.line2, CONSTANTS.PDF_CONSTANTS.kay_x_position, 80);
    for(let i=0;i<CONSTANTS.PDF_CONSTANTS.title.length;i++){
        doc.text(CONSTANTS.PDF_CONSTANTS.title[i], CONSTANTS.PDF_CONSTANTS.kay_x_position, CONSTANTS.PDF_CONSTANTS.y_position+(i*CONSTANTS.PDF_CONSTANTS.line_diff));
    }
    for(let i=0;i<CONSTANTS.PDF_CONSTANTS.key.length;i++){
        doc.text(req.body[CONSTANTS.PDF_CONSTANTS.key[i]], CONSTANTS.PDF_CONSTANTS.value_x_position, CONSTANTS.PDF_CONSTANTS.y_position+(i*CONSTANTS.PDF_CONSTANTS.line_diff));
    }
    // doc.fontSize(10).fillColor('yellow').text(line1, 100, 60);

    doc.end();

    writeStream.on('finish', function () {
        res.sendFile(path.resolve(".")+'/pdf/'+req.body.givenName+'.pdf');
    });
}

exports.getPdf = getPdf;
    